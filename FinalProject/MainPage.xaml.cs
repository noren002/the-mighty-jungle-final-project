﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FinalProject
{
    public partial class MainPage : ContentPage
    {
       
        public MainPage()
        {
            InitializeComponent();
        }
        
        //Create new page when button is clicked
        async void Handle_clicked(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "To-Do List") await Navigation.PushAsync(new To_Do_List());
            if (((Button)sender).Text == "Calendar") await Navigation.PushAsync(new Calendar());
            if (((Button)sender).Text == "Reminders") await Navigation.PushAsync(new Reminders());
        }

    }
}
