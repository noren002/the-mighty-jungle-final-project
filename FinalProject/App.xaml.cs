﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Forms.Xaml;
using static FinalProject.ViewModels.ReminderViewModel;
using Syncfusion.SfSchedule.XForms;

namespace FinalProject
{
    public partial class App : Application
    {

        public static ObservableCollection<ReminderItem> RemindersList { get; set; }
        public static ObservableCollection<ToDoItem> ToDoList { get; set; }
        public static ScheduleAppointmentCollection ScheduleAppointments = new ScheduleAppointmentCollection();

        public App()
        {
            InitializeComponent();
            RemindersList = new ObservableCollection<ReminderItem>();
            ToDoList = new ObservableCollection<ToDoItem>();
            MainPage = new NavigationPage(new MainPage());

        }

        public class ReminderItem
        {
            public TimeSpan time { get; set; }
            public string timestring => time.ToString(@"hh\:mm");
            public string title { get; set; }
            public DateTime day { get; set; }
            public string daystring => day.ToString("yyyy-MM-dd");

            public ReminderItem(TimeSpan Time, string Title, DateTime Day)
            {
                time = Time;
                title = Title;
                day = Day;
                
            }

            public ReminderItem()
            {

            }
        }
        public class ToDoItem
        {
            public string task { get; set; }
            public DateTime day { get; set; }
            public int listGroup { get; set; }
            public bool isChecked { get; set; }
            public string listHeader { get; set; }
            public ToDoItem(string Task, DateTime Day)
            {
                task = Task;
                day = Day;
                isChecked = false;
                listGroup = (int)(Day.Date - DateTime.Now.Date).TotalDays;
                getHeader();


            }
            public void getHeader()
            {
                if (listGroup == 0)
                    listHeader = "Today";
                else if (listGroup == 1)
                    listHeader = "Tomorrow";
                else if (listGroup <= 7)
                    listHeader = "This Week";
                else if (listGroup <= 14)
                    listHeader = "2 Weeks";
                else
                    listHeader = "2+ Weeks";
            }

        }


        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        
    }
}
