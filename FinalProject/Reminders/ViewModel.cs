﻿
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace FinalProject.ViewModels
{
    public class ReminderViewModel
    {
        public App.ReminderItem SelectedReminder { get; set; }
        public ICommand RemoveReminder => new Command(Remove_Reminder);

        public ReminderViewModel()
        {
           
        }

        public void Remove_Reminder()
        {
            App.RemindersList.Remove(SelectedReminder);
        }

       

    }
}