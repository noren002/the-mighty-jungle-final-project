﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FinalProject;
using Xamarin.Forms;
using static FinalProject.ViewModels.ReminderViewModel;

namespace FinalProject
{
    public partial class Reminders : ContentPage
    {

        public Reminders()
        {
            InitializeComponent();
            ItemList.ItemsSource = App.RemindersList;
        }

        async void Handle_Details(object Sender, System.EventArgs e)
        {
            

            await Navigation.PushAsync(new ReminderDetailsPage());
        }

       

    }
}
