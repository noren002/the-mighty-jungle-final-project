﻿using System;
using FinalProject.ViewModels;
using Xamarin.Forms;

namespace FinalProject
{
    public partial class ReminderDetailsPage : ContentPage
    {
        public ReminderDetailsPage()
        {
            InitializeComponent();
            
        }

        void SaveDetails(object Sender, System.EventArgs e)
        {
            App.ReminderItem item = ((EditReminderViewModel)BindingContext).EditReminderItem;
            App.RemindersList.Add(item);
            if (item.time.TotalMinutes - DateTime.Now.TimeOfDay.TotalMinutes > 0)
            {
                Device.StartTimer(item.time - DateTime.Now.TimeOfDay, () =>
                {
                    Application.Current.MainPage.DisplayAlert($"{item.title}", $"{item.timestring}", "Ok");
                    return false;
                });
            }
            

            Navigation.PopAsync();
        }

    }
}
