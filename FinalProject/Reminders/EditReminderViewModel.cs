﻿using System;
using static FinalProject.ViewModels.ReminderViewModel;

namespace FinalProject.ViewModels
{
    public class EditReminderViewModel
    {
        public App.ReminderItem EditReminderItem { get; set; }

        public EditReminderViewModel()
        {
            EditReminderItem = new App.ReminderItem();
        }
    }
}
