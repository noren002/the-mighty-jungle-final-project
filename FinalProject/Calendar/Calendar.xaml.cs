﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Syncfusion.SfSchedule.XForms;



namespace FinalProject
{
    
    public partial class Calendar : ContentPage
    {
        
        public Calendar()
        {
            InitializeComponent();
            schedule.DataSource = App.ScheduleAppointments;
            
        }
      

        private void Button_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(todo.Text) && schedule.SelectedDate != null)
            {

                DateTime d1 = (DateTime)schedule.SelectedDate;
                DateTime d2 = d1;
                double hour = timepicker.Time.Hours;
                double min = timepicker.Time.Minutes;
                double endh = ending.Time.Hours;
                double endm = ending.Time.Minutes;
                App.ScheduleAppointments.Add(new ScheduleAppointment()
               
                {
                    StartTime = d1.AddHours(hour).AddMinutes(min),
                    EndTime = d2.AddHours(endh).AddHours(endm),
                    Subject = todo.Text
                });
                schedule.DataSource = App.ScheduleAppointments;
            }
        }

        private void timepicker_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }
    }
}
