﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FinalProject
{
    public partial class Create_ToDo : ContentPage
    {
        public Create_ToDo()
        {
            InitializeComponent();
        }

        async void Handle_Clicked(object Sender, EventArgs e)
        {
            App.ToDoItem item = new App.ToDoItem(editTask.Text, DatePicker.Date);
            App.ToDoList.Add(item);
            await Navigation.PopAsync();

        }
    }
}
