﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace FinalProject
{
    public partial class To_Do_List : ContentPage
    {
        public To_Do_List()
        {
            InitializeComponent();
            ToDoList.ItemsSource = App.ToDoList;

        }

        async void Handle_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Create_ToDo());
        }

        void Handle_Changed(object sender, CheckedChangedEventArgs e)
        {
            CheckBox checkbox = (CheckBox)sender;
            StackLayout ParentStackLayout = (StackLayout)checkbox.Parent;
            Label ToDoLabel = (Label)ParentStackLayout.Children[1];
            if (checkbox.IsChecked == true)
            {
                ToDoLabel.TextDecorations = TextDecorations.Strikethrough;
            }
            else
            {
                ToDoLabel.TextDecorations = TextDecorations.None;
            }

        }
        protected void Is_Refreshing(object sender, EventArgs e)
        {

            for (int i = 0; i < App.ToDoList.Count; i++)
            {
                App.ToDoList[i].listGroup = (int)(App.ToDoList[i].day.Date - DateTime.Now.Date).TotalDays;
                if (App.ToDoList[i].listGroup < 0)
                    App.ToDoList.Remove(App.ToDoList[i]);
                else
                    App.ToDoList[i].getHeader();
            }
            App.ToDoList = new ObservableCollection<App.ToDoItem>(App.ToDoList.OrderBy(x => x.listGroup).ToList());
            ToDoList.ItemsSource = App.ToDoList;
            ToDoList.IsRefreshing = false;
        }

    }
}